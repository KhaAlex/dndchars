package main

//mongo examples https://docs.objectrocket.com/mongodb_go_examples.html

import (
	"context"
	"dndcharacter"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"
)

type Collection interface {
	Find(interface{}) *mgo.Query
	FindId(interface{}) *mgo.Query
	Insert(...interface{}) error
	UpdateId(interface{}, interface{}) error
	RemoveId(interface{}) error
}

type Mongodb struct {
	Collection
	hosts    []string
	username string //avoid use @ in username!!
	password string //avoid use @ in password!!
	database string
	session  *mgo.Session
}

func (mdb *Mongodb) Init() {
	mdb.hosts = strings.Split(*flag.String("dbhost", "127.0.0.1:27017", "a mongodb hostnames comma separated"), ",")
	mdb.username = *flag.String("user", "user", "a mongodb user name avoid using @ symbol")
	mdb.password = *flag.String("password", "password", "a mongodb user password avoid using @ symbol")
	mdb.database = *flag.String("dbname", "DNDChars", "mongo database name")
	isDrop := flag.Bool("drop", false, "drop database")
	flag.Parse()
	var err error
	log.Printf("Initiating database connection Server:%s, User:%s, DBName:%s", mdb.hosts, mdb.username, mdb.database)
	if mdb.session, err = mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    mdb.hosts,
		Username: mdb.username,
		Password: mdb.password,
		Database: mdb.database,
	}); err != nil {
		log.Fatalf("Database connection failed:%s", err)
	}
	if *isDrop {
		err = mdb.session.DB(mdb.database).DropDatabase()
		if err != nil {
			log.Printf("Database %s drop failed:%s", mdb.database, err)
		}
		log.Printf("Database %s drop succesful", mdb.database)
	}
	mdb.Collection = mdb.session.DB(mdb.database).C("Characters")
}

func (mdb *Mongodb) Close() {
	mdb.session.Close()
	log.Printf("DB session %s closed", mdb.hosts)
}

func (mdb *Mongodb) GetCharacters(rw http.ResponseWriter, req *http.Request) {
	var results []dndcharacter.Character
	if err := mdb.Find(nil).All(&results); err != nil {
		log.Println(err)
	}
	if err := json.NewEncoder(rw).Encode(results); err != nil {
		log.Println(err)
	}
}

func (mdb *Mongodb) GetCharacter(rw http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	var result dndcharacter.Character
	if err := mdb.FindId(bson.ObjectIdHex(params["id"])).One(&result); err != nil {
		log.Println(err)
	}
	json.NewEncoder(rw).Encode(result)
}

func (mdb *Mongodb) CreateCharacter(rw http.ResponseWriter, req *http.Request) {
	var character dndcharacter.Character
	if err := json.NewDecoder(req.Body).Decode(&character); err != nil {
		log.Println(err)
		fmt.Fprintln(rw, err)
		return
	}
	character.Id = bson.NewObjectId()
	mdb.Insert(character)
	json.NewEncoder(rw).Encode(character)
}

func (mdb *Mongodb) UpdateCharacter(rw http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	var changed dndcharacter.Character
	if err := json.NewDecoder(req.Body).Decode(&changed); err != nil {
		log.Println(err)
		fmt.Fprintln(rw, err)
		return
	}
	if err := mdb.UpdateId(bson.ObjectIdHex(params["id"]), changed); err != nil {
		log.Println(err)
		fmt.Fprintln(rw, err)
		return
	}
	fmt.Fprintf(rw, "Character %s update request succesfuly sended.", params["id"])
}

func (mdb *Mongodb) DeleteCharacter(rw http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	if err := mdb.RemoveId(bson.ObjectIdHex(params["id"])); err != nil {
		log.Println(err)
		fmt.Fprintln(rw, err)
		return
	}
	fmt.Fprintf(rw, "Object %s marked for delete", params["id"])
}

func main() {
	var mongo Mongodb
	mongo.Init()
	defer mongo.Close()
	router := mux.NewRouter()
	router.HandleFunc("/chars", mongo.GetCharacters).Methods("GET")
	router.HandleFunc("/chars/{id}", mongo.GetCharacter).Methods("GET")
	router.HandleFunc("/chars", mongo.CreateCharacter).Methods("POST")
	router.HandleFunc("/chars/{id}", mongo.UpdateCharacter).Methods("PUT")
	router.HandleFunc("/chars/{id}", mongo.DeleteCharacter).Methods("DELETE")
	server := &http.Server{Addr: ":8000", Handler: router}
	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Println(err)
	}
}
