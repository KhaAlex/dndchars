package dndcharacter

import (
	"github.com/globalsign/mgo/bson"
)

type Stats struct {
	STR, AGI, CON, INT, WIS, CHA int
}

type Character struct {
	Id    bson.ObjectId `bson:"_id,omitempty"`
	Name  string        `json:"name,omitempty"`
	Stats Stats
}
